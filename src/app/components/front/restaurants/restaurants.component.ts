﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
    RestaurantService,
    LocalStorageService
} from '../../../services/index';
import { Restaurant } from '../../../models/index';

@Component({
    selector: 'restaurants',
    templateUrl: './restaurants.component.html'
})

export class RestaurantsComponent implements OnInit {

    private restaurants: Restaurant[];
    date: string = "2017-05-31";

    constructor(private restaurantService: RestaurantService, private router: Router, private localStorageService: LocalStorageService) { }


    ngOnInit(): void {
        this.getRestaurantsByDate();       
    }

    getRestaurantsByDate(): void {
        this.restaurantService.getRestaurantsByDate(this.date).subscribe((restaurants) => {
            this.restaurants = restaurants;
        });
    }

    dateChange(value: string): void {
        this.date = value;
        this.getRestaurantsByDate();
    }

    // TODO: remove - only for devel testing
    clearStorage(): void {
        if (this.localStorageService.load(this.localStorageService.RESTAURANTS_KEY) !== null) {
            this.localStorageService.clear();
        }        
    }
    

}