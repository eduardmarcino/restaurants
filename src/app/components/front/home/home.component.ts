﻿import { Component, OnInit ,DoCheck } from '@angular/core';
import { AdminUser } from '../../../models/index';
import { LocalStorageService } from '../../../services/index';

@Component({
    selector: 'my-app',
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

    adminUser: AdminUser;

    constructor(private localStorageService: LocalStorageService) { }

    ngOnInit(): void {
 
    }

    ngDoCheck(): void {
        let lAdminUser = this.localStorageService.load(this.localStorageService.USER_KEY);
        this.adminUser = lAdminUser ? lAdminUser : null;
    }

}
