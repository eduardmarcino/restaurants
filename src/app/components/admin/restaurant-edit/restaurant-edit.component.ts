﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { NgForm } from '@angular/forms';

import { RestaurantService } from '../../../services/index';
import { Restaurant, RestaurantMenu, RestaurantMenuItem } from '../../../models/index';


@Component({
    selector: 'restaurant-edit',
    templateUrl: './restaurant-edit.component.html',
})

export class RestaurantEditComponent implements OnInit{
    
    restaurant: Restaurant;
    selectedMenuItem: RestaurantMenuItem;
    selectedMenu: RestaurantMenu;

    flashMessage: string;

    constructor(private restaurantService: RestaurantService, private router: Router, private route: ActivatedRoute, private location: Location) {

    }

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.restaurantService.getRestaurant(+params["id"]))
            .subscribe((restaurant) => {
                this.restaurant = restaurant[0];
            });
    }

    onSelectMenuItem(menuItem: RestaurantMenuItem, menu: RestaurantMenu) {
        this.selectedMenuItem = menuItem;
        this.selectedMenu = menu;
    }

    save(form: NgForm): void {
        this.restaurantService.updateRestaurantMenu(this.restaurant, form.value.menuId);
        this.flashMessage = form.value.name + " bylo uloženo";
    }


    get static(): string {
        return JSON.stringify(this.restaurant);
    }

    goBack(): void {
        this.location.back();
    }

}