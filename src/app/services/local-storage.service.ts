﻿import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

    public USER_KEY: string = "adminUser";
    public RESTAURANTS_KEY: string = "restaurants";

    /**
     * Save into LocalStorage
     * @param data
     * @param key
     */
    save(key: string, data: any): void {
        localStorage.setItem(key, this.stringifyData(data));
    }

    /**
     * Load from LocalStorage
     * @param key
     */
    load(key: string): any {
        return this.parseData((localStorage.getItem(key)));
    }

    /**
     * Clear local storage
     * @param key
     */
    clear(key?: string): void {
        key ? localStorage.removeItem(key) : localStorage.clear();
    }



    private stringifyData(data: any): string {
        return JSON.stringify(data);
    }

    private parseData(data: string): any {
        return JSON.parse(data);
    }

}