﻿export * from './front/restaurant-detail/restaurant-detail.component';
export * from './front/home/home.component';
export * from './front/restaurants/restaurants.component';

export * from './admin/login/login.admin.component';
export * from './admin/restaurants/restaurants.admin.component';
export * from './admin/restaurant-edit/restaurant-edit.component';