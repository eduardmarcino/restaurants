﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {
    HomeComponent,
    RestaurantDetailComponent,
    RestaurantsComponent,

    LoginAdminComponent,
    RestaurantsAdminComponent,
    RestaurantEditComponent
} from './components/index';

import {
    RestaurantService,
    AuthenticationService,
    LocalStorageService
} from './services/index';
import { AuthGuard } from './guards/index';

import { AppRoutingModule } from './app.routing.module';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpModule,
    ],
    declarations: [
        HomeComponent,
        RestaurantDetailComponent,
        RestaurantsComponent,
        LoginAdminComponent,
        RestaurantsAdminComponent,
        RestaurantEditComponent
    ],
    providers: [
        RestaurantService,
        AuthGuard,
        AuthenticationService,
        LocalStorageService
    ],
    bootstrap: [ HomeComponent ]
})
export class AppModule { }
