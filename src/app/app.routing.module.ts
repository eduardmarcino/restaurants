﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
    HomeComponent,
    RestaurantsComponent,
    RestaurantDetailComponent,

    LoginAdminComponent,
    RestaurantsAdminComponent,
    RestaurantEditComponent
} from './components/index'

import { AuthGuard } from './guards/index';

const routes: Routes = [
    { path: 'admin/login', component: LoginAdminComponent },
    {
        path: 'admin', canActivateChild: [AuthGuard], children: [
            { path: '', component: RestaurantsAdminComponent},
            { path: 'edit/:id', component: RestaurantEditComponent },              
        ]
    },    
    { path: '', component: RestaurantsComponent },
    { path: 'detail/:id', component: RestaurantDetailComponent },
    { path: '**', redirectTo: '' }    
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }