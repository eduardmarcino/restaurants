﻿import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { AdminUser } from '../models/index';
import { LocalStorageService } from './local-storage.service';

@Injectable()
export class AuthenticationService {

    mockUser: AdminUser = { username: "admin", password: "admin" };

    constructor(private http: Http, private localStorageService: LocalStorageService) { }

    login(adminUser: AdminUser) {
        if (adminUser.username == this.mockUser.username && adminUser.password == this.mockUser.password) {
            this.localStorageService.save(this.localStorageService.USER_KEY, adminUser);
            return true;
        }
        return false;
    }

    logout() {
        this.localStorageService.clear(this.localStorageService.USER_KEY);
    }

}