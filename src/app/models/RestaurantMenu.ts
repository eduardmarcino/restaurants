﻿import { RestaurantMenuItem } from './index';

export class RestaurantMenu {
    id: number;
    name: string;
    isValid: boolean;
    date: string;
    items: RestaurantMenuItem[];
}