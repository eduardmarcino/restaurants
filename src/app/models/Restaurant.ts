﻿import { RestaurantMenu } from './index';

export class Restaurant {
    id: number;
    name: string;
    menus: RestaurantMenu[];
}