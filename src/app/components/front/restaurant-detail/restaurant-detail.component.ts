﻿import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { RestaurantService } from '../../../services/index';
import { Restaurant } from '../../../models/index';

@Component({
    selector: 'detail-restaurant',
    templateUrl: './restaurant-detail.component.html'
})

export class RestaurantDetailComponent implements OnInit {

    restaurant: Restaurant;

    constructor(private restaurantService: RestaurantService, private route: ActivatedRoute, private location: Location) {

    }

    ngOnInit(): void {
        this.route.params
            .switchMap((params: Params) => this.restaurantService.getRestaurant(+params["id"]))
            .subscribe((restaurant) => {
                this.restaurant = restaurant[0];
                //console.log(this.restaurant);
            });
    }

    goBack(): void {
        this.location.back();
    }

}