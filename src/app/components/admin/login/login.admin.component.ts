﻿/// <reference path="../restaurant-edit/restaurant-edit.component.ts" />
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { AdminUser } from '../../../models/index';
import { AuthenticationService } from '../../../services/index';

@Component({
    selector: 'login-admin',
    templateUrl: './login.admin.component.html',
})

export class LoginAdminComponent implements OnInit {
    
    adminUser: AdminUser;
    returnUrl: string;
    errorMessage: string;    

    constructor(private authenticationService: AuthenticationService,private router: Router,private route: ActivatedRoute) {
        
    }

    ngOnInit(): void {
        this.returnUrl = this.route.snapshot.queryParams["returnUrl"];
        this.adminUser = {
            username: '',
            password: '',
        }         
    }

    login(form: NgForm): void {
        
        if (this.authenticationService.login(this.adminUser)) {
            this.router.navigate([this.returnUrl]);
        } else {
            this.errorMessage = "Chyba při přihlášení!";
            form.resetForm();
        }
    }

    get static(): string {
        return JSON.stringify(this.adminUser);
    }
}