﻿import { Injectable } from '@angular/core';
import { Response, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Restaurant, RestaurantMenu, RestaurantMenuItem } from '../models/index';
import { LocalStorageService } from './local-storage.service';


@Injectable()
export class RestaurantService {

    private dataUrl: string;

    constructor(private http: Http, private localStorageService: LocalStorageService) {
        this.dataUrl = 'app/services/mock-restaurants.json';
    }

    /**
     * Return all restaurants
     */
    getAllRestaurants(): Observable<Restaurant[]> {
        if (this.localStorageService.load(this.localStorageService.RESTAURANTS_KEY) !== null) {
            let localStorageData = this.localStorageService.load(this.localStorageService.RESTAURANTS_KEY);
            return Observable.of(localStorageData).catch(this.handleError);
        }

        let data = this.http.get(this.dataUrl).map(this.extractData).catch(this.handleError);

        data.subscribe((dataRetrieved) => {
            this.localStorageService.save(this.localStorageService.RESTAURANTS_KEY, dataRetrieved);
        }); 

        return data;
    }

    /**
     * Filtering by date
     * @param date
     */
    getRestaurantsByDate(date: string): Observable<Restaurant[]> {  
        return this.getAllRestaurants()
            .map(restaurants => restaurants.filter
                    (restaurant =>
                        (restaurant.menus = restaurant.menus.filter
                          (
                            menu => (menu.isValid == true && menu.date == date)
                          )
                        )
                    )
                )            
            .catch(this.handleError);
    }

    /**
     * Restaurant detail by id
     * @param id
     */
    getRestaurant(id: number): Observable<Restaurant> {
        return this.getAllRestaurants()
            .map(restaurants => restaurants.filter(restaurant => restaurant.id == id))
            .catch(this.handleError);
    }

    /**
     * Update restaurant
     * @param restaurant
     */
    updateRestaurantMenu(restaurant: Restaurant, menuId: number) {
        try {
            let restaurantsLS: Restaurant[] = this.localStorageService.load(this.localStorageService.RESTAURANTS_KEY);
            let restaurantLS = restaurantsLS.filter(rest => rest.id == restaurant.id)[0];
            let menuLS = restaurantLS.menus.filter(menu => menu.id == menuId)[0];
                        
                for (let key in restaurant.menus) {
                    let menu: RestaurantMenu = <RestaurantMenu><any>restaurant.menus[key];

                    // update menu
                    if (menuLS.id == menu.id) {
                        menuLS.name = menu.name;
                        menuLS.isValid = menu.isValid;
                        menuLS.date = menu.date; 


                        // update items
                        for (let keyI in menu.items) {
                            let menuItem: RestaurantMenuItem = <RestaurantMenuItem><any>menu.items[keyI];
                            for (let keyY in menuLS.items) {
                                let menuItemLS: RestaurantMenuItem = <RestaurantMenuItem><any>menuLS.items[keyY];

                                if (menuItem.id == menuItemLS.id) {
                                    menuItemLS.name = menuItem.name;
                                    menuItemLS.price = menuItem.price;
                                }
                            }                                         
                        }
                    }
                    
                }
          

            this.localStorageService.save(this.localStorageService.RESTAURANTS_KEY, restaurantsLS); 
        }
        catch (e) {
            console.log((<Error>e).stack);
        }        
    }

    /**
     * Extracting data from response
     * @param res
     */
    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

 
    /**
     * Handling errors
     * @param error
     */
    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

}
