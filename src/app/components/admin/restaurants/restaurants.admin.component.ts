﻿import { Component, OnInit } from '@angular/core';
import { RestaurantService } from '../../../services/index';
import { Restaurant } from '../../../models/index';

@Component({
    selector: 'restaurants-admin',
    templateUrl: './restaurants.admin.component.html'
})

export class RestaurantsAdminComponent implements OnInit {

    restaurants: Restaurant[];

    constructor(private restaurantService: RestaurantService) {
        
    }

    ngOnInit(): void {
        this.restaurantService.getAllRestaurants()
            .subscribe((restaurants) => {
                this.restaurants = restaurants;
            });
    }

}