﻿export class RestaurantMenuItem {
    id: number;
    name: string;
    price: number;
    currency: string;
}